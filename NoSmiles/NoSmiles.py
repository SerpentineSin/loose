import pygame
from pygame.locals import *
 
class App:
    def __init__(self):
        self._running = True
        self._display_surf = None
        self.size = self.weight, self.height = 640, 640
        self.smile = pygame.image.load("Smile.png")
        self.smile = pygame.transform.scale(self.smile, self.size)
        self.frown = pygame.image.load("Frown.png")
        self.frown = pygame.transform.scale(self.frown, self.size)
        self.smiling = False
 
    def on_init(self):
        pygame.init()
        self._display_surf = pygame.display.set_mode(self.size, pygame.HWSURFACE | pygame.DOUBLEBUF)
        self._running = True
        self._display_surf.blit(self.smile, (0,0))
        self.on_render()
        self.smiling = True
 
    def on_event(self, event):
        if event.type == pygame.QUIT:
            self._running = False
        elif event.type == pygame.MOUSEBUTTONDOWN:
            self.on_click()

    def on_loop(self):
        pass
    def on_render(self):
        pygame.display.flip()
        pass
    def on_cleanup(self):
        pygame.quit()
    def on_click(self):
        self._mouse_pos = pygame.mouse.get_pos()
        if((self._mouse_pos[0] <= 640) and (self._mouse_pos[1] <= 400)):
            if self.smiling == True:
                self._display_surf.blit(self.frown, (0,0))
                self.smiling = not self.smiling
                self.on_render()
            elif self.smiling == False:
                self._display_surf.blit(self.smile, (0,0))
                self.smiling = not self.smiling
                self.on_render()


    
    def on_execute(self):
        if self.on_init() == False:
            self._running = False
 
        while( self._running ):
            for event in pygame.event.get():
                self.on_event(event)
            self.on_loop()
            self.on_render()
        self.on_cleanup()
 
if __name__ == "__main__" :
    theApp = App()
    theApp.on_execute()
